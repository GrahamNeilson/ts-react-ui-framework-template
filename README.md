# TS React UI Framework Template

Typeescript setup for a React UI Components project.

## Usage

```
npm i
npm run build
```

Package is then installable to a Typescript or Javascript React project
